﻿using albelli_assessment.Models;
using albelli_assessment.Models.DTO;
using albelli_assessment.Repository.Interface;
using albelli_assessment.Services.Interface;
using albelli_assessment.Exceptions;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace albelli_assessment.Services
{
    public class OrderService : IOrderService
    {
        private IMapper mapper;
        private IOrderRepository orderRepository;
        private ICustomerRepository customerRepository;
        public OrderService(IMapper mapper, IOrderRepository orderRepository, ICustomerRepository customerRepository)
        {
            this.mapper = mapper;
            this.orderRepository = orderRepository;
            this.customerRepository = customerRepository;
        }
        public async Task<Order> GetOrderAsync(string orderId)
        {
            try
            {
                OrderDTO orderDTO = await orderRepository.GetOrderByOrderIdAsync(orderId);
                Order order = mapper.Map<Order>(orderDTO);
                return order;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<Order>> GetOrderListAsync()
        {
            try
            {
                var orderDTOList = await orderRepository.GetOrderListAsync();
                var orderList = mapper.Map<List<Order>>(orderDTOList);
                return orderList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Order> CreateOrderAsync(Order order)
        {
            try
            {
                if (await IsCustomerExist(order.CustomerId))
                {
                    var orderDTO = mapper.Map<OrderDTO>(order);
                    await orderRepository.CreateAsync(orderDTO);
                    return order;
                }
                else
                {
                    throw new ResourceNotFoundException(String.Format("customer_id: {0} is not exist", order.CustomerId));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<bool> IsCustomerExist(string customerId)
        {
            var customerDTO = await customerRepository.GetCustomerByCustomerIdAsync(customerId);
            return customerDTO != null;
        }
    }
}
