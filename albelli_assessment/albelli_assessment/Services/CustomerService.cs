﻿using albelli_assessment.Models;
using albelli_assessment.Models.DTO;
using albelli_assessment.Repository.Interface;
using albelli_assessment.Services.Interface;
using albelli_assessment.Exceptions;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace albelli_assessment.Services
{
    public class CustomerService : ICustomerService
    {
        private IMapper mapper;
        private ICustomerRepository customerRepository;
        private IOrderRepository orderRepository;
        public CustomerService(IMapper mapper, ICustomerRepository customerRepository, IOrderRepository orderRepository)
        {
            this.mapper = mapper;
            this.customerRepository = customerRepository;
            this.orderRepository = orderRepository;
        }

        public async Task<Customer> GetCustomerAsync(string customerId)
        {
            try
            {
                var customerDTO = await customerRepository.GetCustomerByCustomerIdAsync(customerId);
                var orderDTOList = await orderRepository.GetOrderListByCustomerIdAsync(customerId);
                if (customerDTO != null)
                {
                    var customer = mapper.Map<Customer>(customerDTO);
                    customer.OrderList = mapper.Map<List<Order>>(orderDTOList);
                    return customer;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<Customer>> GetCustomerListAsync()
        {
            try
            {
                var customerDTOList = await customerRepository.GetCustomerListAsync();
                var customerList = mapper.Map<List<Customer>>(customerDTOList);
                return customerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Customer> CreateCustomerAsync(Customer customer)
        {
            try
            {
                var customerFromExist = await GetCustomerAsync(customer.CustomerId);
                if (customerFromExist == null)
                {
                    var customerDTO = mapper.Map<CustomerDTO>(customer);
                    await customerRepository.CreateAsync(customerDTO);
                    return customer;
                }
                else
                {
                    throw new ResourceConflictException(customer);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
