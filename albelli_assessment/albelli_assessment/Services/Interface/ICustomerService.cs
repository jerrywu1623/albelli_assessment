﻿using albelli_assessment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace albelli_assessment.Services.Interface
{
    public interface ICustomerService
    {
        Task<Customer> GetCustomerAsync(string customerId);
        Task<IEnumerable<Customer>> GetCustomerListAsync();
        Task<Customer> CreateCustomerAsync(Customer customer);
    }
}
