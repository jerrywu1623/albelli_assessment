﻿using albelli_assessment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace albelli_assessment.Services.Interface
{
    public interface IOrderService
    {
        Task<Order> GetOrderAsync(string orderId);
        Task<IEnumerable<Order>> GetOrderListAsync();
        Task<Order> CreateOrderAsync(Order order);
    }
}
