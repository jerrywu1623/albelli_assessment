﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace albelli_assessment.Exceptions
{
    public class ResourceNotFoundException : BaseException
    {
        private string ResponseBodyText;

        public ResourceNotFoundException(string message) : base(HttpStatusCode.NotFound)
        {
            ResponseBodyText = message;
        }

        public override string GetResponseBody()
        {
            return ResponseBodyText;
        }
    }
}
