﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;

namespace albelli_assessment.Exceptions
{
    public class UnExpectedException : BaseException
    {
        private const string ResponseBodyText = "server_error";

        public UnExpectedException(Exception exception) : base(exception, HttpStatusCode.InternalServerError)
        { }

        public override string GetResponseBody()
        {
            return ResponseBodyText;
        }
    }
}
