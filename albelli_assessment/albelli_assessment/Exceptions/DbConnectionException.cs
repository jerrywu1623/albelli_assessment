﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace albelli_assessment.Exceptions
{
    public class DbConnectionException : BaseException
    {
        private const string ResponseBodyText = "server_db_connection_error";

        public DbConnectionException(Exception exception) : base(exception, HttpStatusCode.InternalServerError)
        {}

        public override string GetResponseBody()
        {
            return ResponseBodyText;
        }
    }
}
