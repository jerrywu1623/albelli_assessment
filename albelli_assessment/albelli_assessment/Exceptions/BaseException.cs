﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace albelli_assessment.Exceptions
{
    public class BaseException : Exception
    {
        public HttpStatusCode StatusCode { get; }
        private string ResponsdeBodyText;

        public BaseException(): base()
        {

        }

        public BaseException(HttpStatusCode statusCode) : base()
        {
            StatusCode = statusCode;
        }

        public BaseException(Exception exception, HttpStatusCode statusCode, string errorMessage)
            : base(errorMessage, exception)
        {
            StatusCode = statusCode;
        }

        public BaseException(Exception exception, HttpStatusCode statusCode)
            : base (exception.Message, exception)
        {
            StatusCode = statusCode;
        }

        public virtual string GetResponseBody()
        {
            return ResponsdeBodyText;
        }
        
    }
}
