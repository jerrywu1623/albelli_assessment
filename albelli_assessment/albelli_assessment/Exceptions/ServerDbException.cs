﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace albelli_assessment.Exceptions
{
    public class ServerDbException : BaseException
    {
        private const string ResponseBodyText = "server_db_error";

        public ServerDbException(Exception exception) : base(exception, HttpStatusCode.InternalServerError)
        { }

        public override string GetResponseBody()
        {
            return ResponseBodyText;
        }
    }
}
