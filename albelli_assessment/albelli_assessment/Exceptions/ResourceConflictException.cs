﻿using albelli_assessment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace albelli_assessment.Exceptions
{
    public class ResourceConflictException : BaseException
    {
        private string ResponseBodyText;
        public ResourceConflictException(Customer customer) : base(HttpStatusCode.Conflict)
        {
            ResponseBodyText = JsonConvert.SerializeObject(customer);
        }
        public ResourceConflictException(Order order) : base(HttpStatusCode.Conflict)
        {
            ResponseBodyText = JsonConvert.SerializeObject(order);
        }

        public override string GetResponseBody()
        {
            return ResponseBodyText;
        }
    }
}
