﻿using albelli_assessment.Models;
using albelli_assessment.Models.DTO;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace albelli_assessment.AutoMapperProfiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CustomerDTO, Customer>().ReverseMap();
            CreateMap<OrderDTO, Order>().ReverseMap();
        }
    }
}
