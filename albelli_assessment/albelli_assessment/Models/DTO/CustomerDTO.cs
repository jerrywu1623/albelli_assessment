﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace albelli_assessment.Models.DTO
{
    public class CustomerDTO
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("customer_id")]
        public string CustomerId { get; set; }
        [BsonElement("name")]
        public string Name { get; set; }
        [BsonElement("email")]
        public string Email { get; set; }
        [BsonElement("created_date")]
        public DateTime CreatedDate { get; set; }
        [BsonElement("modify_date")]
        public DateTime ModifyDate { get; set; }
    }
}
