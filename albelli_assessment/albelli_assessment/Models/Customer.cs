﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace albelli_assessment.Models
{
    public class Customer
    {
        [JsonProperty("customer_id")]
        [Required(ErrorMessage = "customer_id is missing")]        
        public string CustomerId { get; set; }
        [JsonProperty("name")]
        [Required(ErrorMessage = "name is missing")]        
        public string Name { get; set; }
        [JsonProperty("email")]
        [Required(ErrorMessage = "email is missing")]        
        [EmailAddress]
        public string Email { get; set; }
        [JsonProperty("order_list")]
        public List<Order> OrderList { get; set; }
    }
}
