﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace albelli_assessment.Models
{
    public class Order
    {
        [JsonProperty("order_id")]
        [Required(ErrorMessage = "order_id is missing")]
        public string OrderId { get; set; }
        [JsonProperty("customer_id")]
        [Required(ErrorMessage = "customer_id is missing")]
        public string CustomerId { get; set; }
        [JsonProperty("price")]
        public double Price { get; set; }
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
