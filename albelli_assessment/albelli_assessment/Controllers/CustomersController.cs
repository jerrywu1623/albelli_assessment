﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using albelli_assessment.Models;
using albelli_assessment.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace albelli_assessment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private ICustomerService customerService;
        public CustomersController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }
        // GET: api/Customers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Customer>>> GetCustomerList()
        {
            var customerList = await customerService.GetCustomerListAsync();
            return customerList.ToList();
        }

        // GET: api/Customers/5
        [HttpGet("{customerId}")]
        public async Task<ActionResult<Customer>> GetCustomerByCustomerId(string customerId)
        {
            var customer = await customerService.GetCustomerAsync(customerId);
            if (customer != null)
            {
                return customer;
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Customers
        [HttpPost]
        public async Task<ActionResult<Customer>> Post([FromBody]Customer customer)
        {
            if (ModelState.IsValid)
            {
                var createCustomer = await customerService.CreateCustomerAsync(customer);
                return CreatedAtAction(nameof(GetCustomerByCustomerId), new { customerId = createCustomer.CustomerId }, createCustomer);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
    }
}
