﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using albelli_assessment.Models;
using albelli_assessment.Services.Interface;

namespace albelli_assessment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private IOrderService orderService;
        public OrdersController(IOrderService orderService)
        {
            this.orderService = orderService;
        }
        // GET: api/Orders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Order>>> GetOrderList()
        {
            var orderList = await orderService.GetOrderListAsync();
            return orderList.ToList();
        }

        // GET: api/Orders/5
        [HttpGet("{orderId}")]
        public async Task<ActionResult<Order>> GetOrderByOrderId(string orderId)
        {
            var order = await orderService.GetOrderAsync(orderId);
            if (order != null)
            {
                return order;
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Orders
        [HttpPost]
        public async Task<ActionResult<Order>> Post([FromBody]Order order)
        {
            var createOrder = await orderService.CreateOrderAsync(order);
            return CreatedAtAction(nameof(GetOrderByOrderId), new { orderId = createOrder.OrderId }, createOrder);
        }
    }
}
