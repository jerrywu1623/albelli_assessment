﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using albelli_assessment.Exceptions;
using Microsoft.Extensions.Logging;
using albelli_assessment.Models;

namespace albelli_assessment.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private ILogger logger;
        private readonly RequestDelegate next;
        public ExceptionHandlingMiddleware(RequestDelegate next, ILogger<ExceptionHandlingMiddleware> logger)
        {
            this.logger = logger;
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            BaseException baseException;

            if(ex is BaseException)
            {
                baseException = ex as BaseException;
            }
            else
            {
                baseException = new UnExpectedException(ex);
            }

            if((int)baseException.StatusCode > 499)
            {
                logger.LogError(new ErrorLog
                {
                    stack_trace = baseException.StackTrace,
                    message = baseException.Message
                }.ToString());
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)baseException.StatusCode;
            await context.Response.WriteAsync(baseException.GetResponseBody());
        }
    }
}
