﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using albelli_assessment.Models.DTO;
using MongoDB.Driver;

namespace albelli_assessment.Repository.Interface
{
    public interface IMongoDbContext
    {
        IMongoCollection<CustomerDTO> Customers { get; }
        IMongoCollection<OrderDTO> Orders { get; }
    }
}
