﻿using albelli_assessment.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace albelli_assessment.Repository.Interface
{
    public interface IOrderRepository
    {
        Task<IEnumerable<OrderDTO>> GetOrderListAsync();
        Task<IEnumerable<OrderDTO>> GetOrderListByCustomerIdAsync(string customerId);
        Task<OrderDTO> GetOrderByOrderIdAsync(string orderId);
        Task CreateAsync(OrderDTO orderDTO);
        Task<bool> UpdateAsync(OrderDTO orderDTO);
        Task<bool> DeleteAsync(string orderId);
    }
}
