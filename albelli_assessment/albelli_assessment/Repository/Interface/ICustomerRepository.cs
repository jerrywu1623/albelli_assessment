﻿using albelli_assessment.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace albelli_assessment.Repository.Interface
{
    public interface ICustomerRepository
    {
        Task<IEnumerable<CustomerDTO>> GetCustomerListAsync();
        Task<CustomerDTO> GetCustomerByCustomerIdAsync(string customerId);
        Task CreateAsync(CustomerDTO customerDTO);
        Task<bool> UpdateAsync(CustomerDTO customerDTO);
        Task<bool> DeleteAsync(string customerId);
    }
}
