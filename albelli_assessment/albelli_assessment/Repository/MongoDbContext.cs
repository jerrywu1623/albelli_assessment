﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using albelli_assessment.Exceptions;
using albelli_assessment.Models;
using albelli_assessment.Models.DTO;
using albelli_assessment.Repository.Interface;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace albelli_assessment.Repository
{
    public class MongoDbContext : IMongoDbContext
    {
        private readonly IMongoDatabase database = null;
        public MongoDbContext(IOptions<MongoDBSettings> settings)
        {
            try
            {
                var client = new MongoClient(settings.Value.ConnectionString);
                if (client != null)
                {
                    database = client.GetDatabase(settings.Value.Database);
                }
            }
            catch (MongoConnectionException ex)
            {
                throw new DbConnectionException(ex);
            }            
        }
        public IMongoCollection<CustomerDTO> Customers
        {
            get
            {
                return database.GetCollection<CustomerDTO>("customers");
            }
        }
        public IMongoCollection<OrderDTO> Orders
        {
            get
            {
                return database.GetCollection<OrderDTO>("orders");
            }
        }
    }
}
