﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using albelli_assessment.Models.DTO;
using albelli_assessment.Repository.Interface;
using albelli_assessment.Exceptions;

namespace albelli_assessment.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IMongoDbContext mongoDbContext = null;
        public CustomerRepository(IMongoDbContext mongoDbContext)
        {
            this.mongoDbContext = mongoDbContext;
        }
        public async Task<CustomerDTO> GetCustomerByCustomerIdAsync(string customerId)
        {
            try
            {
                FilterDefinition<CustomerDTO> filter = Builders<CustomerDTO>.Filter.Eq(c => c.CustomerId, customerId);
                return await mongoDbContext.Customers.Find(filter).FirstOrDefaultAsync();
            }
            catch (MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }

        public async Task<IEnumerable<CustomerDTO>> GetCustomerListAsync()
        {
            try
            {
                return await mongoDbContext.Customers.Find(_ => true).ToListAsync();
            }
            catch (MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }
        public async Task CreateAsync(CustomerDTO customer)
        {
            try
            {
                customer.CreatedDate = DateTime.UtcNow;
                customer.ModifyDate = DateTime.UtcNow;
                await mongoDbContext.Customers.InsertOneAsync(customer);
            }
            catch(MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }
        public async Task<bool> UpdateAsync(CustomerDTO customer)
        {
            try
            {
                customer.ModifyDate = DateTime.UtcNow;
                ReplaceOneResult updateResult = await mongoDbContext.Customers
                    .ReplaceOneAsync(
                        filter: c => c.CustomerId == customer.CustomerId,
                        replacement: customer
                    );

                return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
            }
            catch (MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }
        public async Task<bool> DeleteAsync(string customerId)
        {
            try
            {
                FilterDefinition<CustomerDTO> filter = Builders<CustomerDTO>.Filter.Eq(c => c.CustomerId, customerId);
                DeleteResult deleteResult = await mongoDbContext.Customers.DeleteOneAsync(filter);

                return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
            }
            catch (MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }
    }
}
