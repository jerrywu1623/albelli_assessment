﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using albelli_assessment.Models.DTO;
using albelli_assessment.Repository.Interface;
using albelli_assessment.Exceptions;

namespace albelli_assessment.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IMongoDbContext mongoDbContext = null;
        public OrderRepository(IMongoDbContext mongoDbContext)
        {
            this.mongoDbContext = mongoDbContext;
        }
        public async Task<OrderDTO> GetOrderByOrderIdAsync(string orderId)
        {
            try
            {
                FilterDefinition<OrderDTO> filter = Builders<OrderDTO>.Filter.Eq(o => o.OrderId, orderId);
                return await mongoDbContext.Orders.Find(filter).FirstOrDefaultAsync();
            }
            catch (MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }
        public async Task<IEnumerable<OrderDTO>> GetOrderListByCustomerIdAsync(string customerId)
        {
            try
            {
                return await mongoDbContext.Orders.Find(o => o.CustomerId == customerId).ToListAsync();
            }
            catch (MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }
        public async Task<IEnumerable<OrderDTO>> GetOrderListAsync()
        {
            try
            {
                return await mongoDbContext.Orders.Find(_ => true).ToListAsync();
            }
            catch (MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }
        public async Task CreateAsync(OrderDTO orderDTO)
        {
            try
            {
                orderDTO.CreatedDate = DateTime.UtcNow;
                orderDTO.ModifyDate = DateTime.UtcNow;
                await mongoDbContext.Orders.InsertOneAsync(orderDTO);
            }
            catch (MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }

        public async Task<bool> UpdateAsync(OrderDTO orderDTO)
        {
            try
            {
                orderDTO.ModifyDate = DateTime.UtcNow;
                ReplaceOneResult updateResult = await mongoDbContext.Orders
                    .ReplaceOneAsync(
                        filter: o => o.OrderId == orderDTO.OrderId,
                        replacement: orderDTO
                    );

                return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
            }
            catch (MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }

        public async Task<bool> DeleteAsync(string orderId)
        {
            try
            {
                FilterDefinition<OrderDTO> filter = Builders<OrderDTO>.Filter.Eq(o => o.OrderId, orderId);
                DeleteResult deleteResult = await mongoDbContext.Orders.DeleteOneAsync(filter);

                return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
            }
            catch (MongoServerException ex)
            {
                throw new ServerDbException(ex);
            }
            catch (Exception ex)
            {
                throw new UnExpectedException(ex);
            }
        }
    }
}
