﻿using NUnit.Framework;
using albelli_assessment.Models;
using albelli_assessment.Services.Interface;
using albelli_assessment.Services;
using albelli_assessment.Repository.Interface;
using albelli_assessment.Repository;
using NSubstitute;
using Moq;
using AutoMapper;
using albelli_assessment.AutoMapperProfiles;
using System.Collections.Generic;
using albelli_assessment.Models.DTO;
using System.Linq;

namespace albelli_assessment.test.Services
{
    [TestFixture]
    public class CustomerServiceTest
    {
        ICustomerService customerService;
        ICustomerRepository customerRepository;
        IOrderRepository orderRepository;
        IMapper mapper;
        [SetUp]
        public void Setup()
        {
            customerRepository = Substitute.For<ICustomerRepository>();
            orderRepository = Substitute.For<IOrderRepository>();
            mapper = new MapperConfiguration(config =>
              {
                  config.AddProfile(new MappingProfile());
              }).CreateMapper();

            customerService = new CustomerService(mapper, customerRepository, orderRepository);
        }

        [Test]
        public void GetCustomerList_ok()
        {
            //arrange
            List<CustomerDTO> customerDTOList = new List<CustomerDTO>()
            {
                new CustomerDTO{ CustomerId="abc", Name="Jerry", Email="jerry.wu1623@gmail.com"},
                new CustomerDTO{ CustomerId="def", Name="John", Email="john_wu@gmail.com"}
            };

            customerRepository.GetCustomerListAsync().Returns(customerDTOList);

            //act
            var actual = customerService.GetCustomerListAsync();

            //assert
            Assert.IsTrue(actual.Result.Count() > 0);
        }

        [Test]
        public void GetCustomer_ok()
        {
            //arrange
            CustomerDTO customerDTO = new CustomerDTO { CustomerId = "abc", Name = "Jerry", Email = "jerry_wu@gmail.com" };
            List<OrderDTO> orderDTOList = new List<OrderDTO>()
            {
                new OrderDTO { OrderId = "o1", CustomerId = "abc", Price = 20.0}
            };
            customerRepository.GetCustomerByCustomerIdAsync(Arg.Any<string>()).Returns(customerDTO);
            orderRepository.GetOrderListByCustomerIdAsync(Arg.Any<string>()).Returns(orderDTOList);

            //act
            var actual = customerService.GetCustomerAsync("abc");

            //assert
            Assert.AreEqual(customerDTO.CustomerId, actual.Result.CustomerId);
            Assert.AreEqual(customerDTO.Name, actual.Result.Name);
            Assert.AreEqual(customerDTO.Email, actual.Result.Email);
        }

        [Test]
        public void CreateCustomer_ok()
        {
            //arrange
            Customer customer = new Customer
            {
                CustomerId = "aaa",
                Name = "Leo",
                Email = "leo@gmail.com"
            };

            //act
            var actual = customerService.CreateCustomerAsync(customer);

            //assert
            Assert.AreEqual(customer.CustomerId, actual.Result.CustomerId);
            Assert.AreEqual(customer.Name, actual.Result.Name);
            Assert.AreEqual(customer.Email, actual.Result.Email);
        }
    }
}
