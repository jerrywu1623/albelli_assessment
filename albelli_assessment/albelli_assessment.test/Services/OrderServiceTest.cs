﻿using albelli_assessment.AutoMapperProfiles;
using albelli_assessment.Exceptions;
using albelli_assessment.Models;
using albelli_assessment.Models.DTO;
using albelli_assessment.Repository.Interface;
using albelli_assessment.Services;
using albelli_assessment.Services.Interface;
using AutoMapper;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace albelli_assessment.test.Services
{
    [TestFixture]
    public class OrderServiceTest
    {
        IOrderService orderService;
        ICustomerRepository customerRepository;
        IOrderRepository orderRepository;
        IMapper mapper;
        [SetUp]
        public void Setup()
        {
            customerRepository = Substitute.For<ICustomerRepository>();
            orderRepository = Substitute.For<IOrderRepository>();
            mapper = new MapperConfiguration(config =>
            {
                config.AddProfile(new MappingProfile());
            }).CreateMapper();

            orderService = new OrderService(mapper, orderRepository, customerRepository);
        }

        [Test]
        public void GetOrderList_ok()
        {
            //arrange
            List<OrderDTO> orderDTOList = new List<OrderDTO>
            {
                new OrderDTO { OrderId = "a1", CustomerId = "abc", Price = 20.0 },
                new OrderDTO { OrderId = "b1", CustomerId = "def", Price = 12.5 }
            };
            orderRepository.GetOrderListAsync().Returns(orderDTOList);

            //act
            var actual = orderService.GetOrderListAsync();

            //assert
            Assert.IsTrue(actual.Result.Count() > 0);
        }

        [Test]
        public void GetOrder_ok()
        {
            //arrange
            OrderDTO orderDTO = new OrderDTO
            {
                OrderId = "a1",
                CustomerId = "abc",
                Price = 20.0
            };
            orderRepository.GetOrderByOrderIdAsync(Arg.Any<string>()).Returns(orderDTO);

            //act
            var actual = orderService.GetOrderAsync("a1");

            //assert
            Assert.AreEqual(orderDTO.OrderId, actual.Result.OrderId);
            Assert.AreEqual(orderDTO.CustomerId, actual.Result.CustomerId);
            Assert.AreEqual(orderDTO.Price, actual.Result.Price);
        }

        [Test]
        public void CreateOrder_ok()
        {
            //arrange
            Order order = new Order
            {
                OrderId = "c1",
                CustomerId = "abc",
                Price = 13.0
            };
            CustomerDTO customerDTO = new CustomerDTO
            {
                CustomerId = "abc"
            };
            customerRepository.GetCustomerByCustomerIdAsync(Arg.Any<string>()).Returns(customerDTO);

            //act
            var actual = orderService.CreateOrderAsync(order);

            //assert
            Assert.AreEqual(order.OrderId, actual.Result.OrderId);
            Assert.AreEqual(order.CustomerId, actual.Result.CustomerId);
            Assert.AreEqual(order.Price, actual.Result.Price);
        }
    }
}
